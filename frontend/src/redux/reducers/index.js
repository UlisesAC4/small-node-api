const initialState = {
  company: {},
  offices: {}
};

const rootReducer = (state = initialState, actions) => {
  switch (actions.type) {
    case "CREATE_COMPANY":
      return Object.assign({}, state, {
        company: { id: actions.companyData.id, name: actions.companyData.name }
      });
    default:
      return state;
  }
};

export default rootReducer;
