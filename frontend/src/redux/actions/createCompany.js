const createCompany = companyData => ({
  type: "CREATE_COMPANY",
  companyData
});

export default createCompany;
