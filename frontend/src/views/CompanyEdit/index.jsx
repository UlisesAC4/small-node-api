import React, { Component } from "react";
import { connect } from "react-redux";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Layout from "../../components/layout";
import API from "../../api";
import Button from "@material-ui/core/Button";
import OutlinedInput from "@material-ui/core/OutlinedInput";

function mapStateToProps(state) {
  return { company: state.company };
}

class EditCompany extends Component {
  constructor(props) {
    super(props);
    this.state = {
      company: { headquarter: null },
      offices: [],
      selectedHeadquarter: ""
    };
  }
  componentDidMount = async () => {
    const company = API.Companies.show(this.props.company.id);
    const offices = API.Offices.showAll(this.props.company.id);
    const data = await Promise.all([company, offices]);

    this.setState({ company: data[0], offices: data[1] });
  };
  render() {
    return (
      <Layout>
        <h2>Edition of company</h2>
        <p>Select the company then submit it</p>
        <Select
          value={this.state.selectedHeadquarter}
          onChange={event => {
            this.setState({ selectedHeadquarter: event.target.value });
          }}
          input={
            <OutlinedInput
              labelWidth={this.state.labelWidth}
              name="appointment-state"
              id="outlined-appointment-state-simple"
            />
          }
        >
          {this.state.offices.map(office => {
            return (
              <MenuItem value={office.id} key={office.id}>{`${office.street}, ${
                office.postalCode
              }`}</MenuItem>
            );
          })}
        </Select>
        <br />
        <br />
        <Button
          variant="contained"
          color="primary"
          onClick={async () => {
            const response = await API.Companies.update(
              this.state.selectedHeadquarter,
              this.props.company.id
            );
            this.props.history.push({ pathname: "/company/details" });
          }}
        >
          Submit
        </Button>
      </Layout>
    );
  }
}

export default connect(mapStateToProps)(EditCompany);
