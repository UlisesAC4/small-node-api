import React, { Component } from "react";
import { connect } from "react-redux";
import Actions from "../../redux/actions";
import Layout from "../../components/layout";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

import API from "../../api";

const mapDispatchToProps = dispatch => {
  return {
    companyCreate: data => dispatch(Actions.createCompany(data))
  };
};

class CreateCompany extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      offices: [],
      tempStreet: "",
      tempPostalCode: "",
      tempCity: "",
      tempMonthlyRent: "",
      openSnackBar: false
    };
  }
  addOffice = () => {
    this.setState({
      offices: [
        ...this.state.offices,
        {
          street: this.state.tempStreet,
          postalCode: this.state.tempPostalCode,
          city: this.state.tempCity,
          monthlyRent: this.state.tempMonthlyRent
        }
      ],
      tempStreet: "",
      tempPostalCode: "",
      tempCity: "",
      tempMonthlyRent: ""
    });
  };
  submitCompany = async () => {
    try {
      const response = await API.Companies.create(
        this.state.name,
        this.state.offices
      );
      // use redux to save companmy info
      this.props.companyCreate(response);
      this.props.history.push({
        pathname: "/company/details",
        state: { message: "Company created" }
      });
    } catch (error) {
      this.handleSnackbar();
    }
  };
  handleSnackbar = () => {
    this.setState({ openSnackBar: !this.state.openSnackBar });
  };
  render() {
    return (
      <Layout>
        <h2>Company creation</h2>
        <TextField
          variant="outlined"
          label="Company name"
          value={this.state.name}
          onChange={event => {
            this.setState({ name: event.target.value });
          }}
        />
        <p>Data for offices creation:</p>
        <Grid container>
          <Grid item xs>
            <TextField
              variant="outlined"
              label="Street"
              value={this.state.tempStreet}
              onChange={event => {
                this.setState({ tempStreet: event.target.value });
              }}
            />
          </Grid>
          <Grid item xs>
            <TextField
              variant="outlined"
              label="Postal Code"
              value={this.state.tempPostalCode}
              onChange={event => {
                this.setState({ tempPostalCode: event.target.value });
              }}
            />
          </Grid>
          <Grid item xs>
            <TextField
              variant="outlined"
              label="City"
              value={this.state.tempCity}
              onChange={event => {
                this.setState({ tempCity: event.target.value });
              }}
            />
          </Grid>
          <Grid item xs>
            <TextField
              variant="outlined"
              label="Monthly rent"
              value={this.state.tempMonthlyRent}
              onChange={event => {
                this.setState({ tempMonthlyRent: event.target.value });
              }}
            />
          </Grid>
          <Grid item xs>
            <Button onClick={this.addOffice}>Add</Button>
          </Grid>
        </Grid>
        <h3>Your current offices:</h3>
        {this.state.offices.length > 0 ? (
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Street</TableCell>
                <TableCell>City</TableCell>
                <TableCell>Postal Code</TableCell>
                <TableCell>Monthly Rent</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.offices.map(office => {
                return (
                  <TableRow>
                    <TableCell>{office.street}</TableCell>
                    <TableCell>{office.city}</TableCell>
                    <TableCell>{office.postalCode}</TableCell>
                    <TableCell>{office.monthlyRent}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        ) : (
          <p>You have not put information to create offices.</p>
        )}
        <br />
        <Button
          variant="contained"
          color="primary"
          onClick={this.submitCompany}
        >
          Submit
        </Button>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={this.state.openSnackBar}
          autoHideDuration={6000}
          onClose={this.openSnackBar}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={
            <span id="message-id">There was an error, try it later</span>
          }
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={this.openSnackBar}
            >
              <CloseIcon />
            </IconButton>
          ]}
        />
      </Layout>
    );
  }
}

export default connect(
  null,
  mapDispatchToProps
)(CreateCompany);
