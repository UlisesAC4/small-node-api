import React, { Component } from "react";
import { connect } from "react-redux";
import Layout from "../../components/layout";
import API from "../../api";

import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

function mapStateToProps(state) {
  return { company: state.company };
}

class CompanyDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      company: { headquarter: null },
      offices: []
    };
  }
  componentDidMount = async () => {
    const company = API.Companies.show(this.props.company.id);
    const offices = API.Offices.showAll(this.props.company.id);
    const data = await Promise.all([company, offices]);

    this.setState({ company: data[0], offices: data[1] });
  };
  render() {
    return (
      <Layout>
        <h2>Company details</h2>
        {this.state.company.name ? (
          <div>
            <p>
              <strong>Name: </strong>
              {this.state.company.name}
            </p>
            <p>
              <strong>Total rent: </strong>
              {this.state.company.totalRent.totalRent}
            </p>
          </div>
        ) : (
          <p>There is no info of this company at the moment.</p>
        )}
        <h3>Headquarter details</h3>
        {this.state.company.headquarter ? (
          <div>
            <p>
              <strong>Street: </strong>
              {this.state.company.headquarter.street}
            </p>
            <p>
              <strong>Postal code: </strong>
              {this.state.company.headquarter.postalCode}
            </p>
            <p>
              <strong>City: </strong>
              {this.state.company.headquarter.city}
            </p>
          </div>
        ) : (
          <div>
            <p>
              There is no info of Headquarters, either there is not one set or
              there is a problem in the connection right now.
            </p>
          </div>
        )}
        <h3>Offices</h3>
        {this.state.offices.length > 0 ? (
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Street</TableCell>
                <TableCell>City</TableCell>
                <TableCell>Postal Code</TableCell>
                <TableCell>Monthly Rent</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.offices.map(office => {
                return (
                  <TableRow>
                    <TableCell>{office.street}</TableCell>
                    <TableCell>{office.city}</TableCell>
                    <TableCell>{office.postalCode}</TableCell>
                    <TableCell>{office.monthlyRent}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        ) : (
          <p>There are not offices to show</p>
        )}
      </Layout>
    );
  }
}

export default connect(mapStateToProps)(CompanyDetails);
