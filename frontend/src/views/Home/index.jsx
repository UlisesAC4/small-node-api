import React from "react";
import Layout from "../../components/layout";

const Home = () => {
  return (
    <Layout>
      <h2>Home page</h2>
      <p>
        Welcome to the home page of this project, to start using this project go
        to create page in the navbar.
      </p>
    </Layout>
  );
};

export default Home;
