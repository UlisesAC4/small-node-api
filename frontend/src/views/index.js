import Home from "./Home";
import CompanyCreate from "./CompanyCreate";
import CompanyDetails from "./CompanyDetails";
import CompanyEdit from "./CompanyEdit";

export default { Home, CompanyCreate, CompanyDetails, CompanyEdit };
