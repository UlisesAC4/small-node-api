import React from "react";
import Views from "./views";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

const RouterWrapper = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Views.Home} />
        <Route path="/company/create" exact component={Views.CompanyCreate} />
        <Route path="/company/details" exact component={Views.CompanyDetails} />
        <Route path="/company/edit" exact component={Views.CompanyEdit} />
      </Switch>
    </Router>
  );
};

export default RouterWrapper;
