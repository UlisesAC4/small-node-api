import Constants from "../../utils/constants";

const show = async officeId => {
  const response = await fetch(`${Constants.BASE_URL}companies/${officeId}`, {
    method: "GET"
  });
  return await response.json();
};

export default show;
