import Constants from "../../utils/constants";

const create = async (name, offices) => {
  const response = await fetch(`${Constants.BASE_URL}companies/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ name, offices })
  });
  return await response.json();
};

export default create;
