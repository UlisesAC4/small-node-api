import create from "./create";
import show from "./show";
import update from "./update";

export default { create, show, update };
