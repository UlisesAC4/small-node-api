import Constants from "../../utils/constants";

const update = async (headquarterId, companyId) => {
  const response = await fetch(`${Constants.BASE_URL}companies/${companyId}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ officeId: headquarterId })
  });
  return await response.json();
};

export default update;
