import Constants from "../../utils/constants";

const showAll = async companyId => {
  const response = await fetch(
    `${Constants.BASE_URL}companies/${companyId}/offices`,
    {
      method: "GET"
    }
  );
  return await response.json();
};

export default showAll;
