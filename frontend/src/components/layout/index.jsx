import React, { Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";

const Layout = props => {
  return (
    <div>
      <header>
        <nav>
          <AppBar position="static">
            <Toolbar>
              <Button color="inherit">
                <Link to="/">Home</Link>
              </Button>
              <Button color="inherit">
                <Link to="/company/create">Create</Link>
              </Button>
              <Button color="inherit">
                <Link to="/company/edit">Edit</Link>
              </Button>
              <Button color="inherit">
                <Link to="/company/details">Show</Link>
              </Button>
            </Toolbar>
          </AppBar>
        </nav>
      </header>

      <section style={{ marginTop: "32px", marginBottom: "32px" }}>
        <Grid container style={{ maxWidth: "70%" }} />
        {props.children}
      </section>
    </div>
  );
};

export default Layout;
