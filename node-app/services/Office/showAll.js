const Models = require("../../models");
const Company = Models.company;
const Office = Models.office;

module.exports = async companyId => {
  const company = await Company.findOne({
    where: { id: companyId },
    include: [{ model: Office }]
  });
  return company.offices;
};
