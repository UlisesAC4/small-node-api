const Company = require("./Company");
const Office = require("./Office");

module.exports = {
  Company,
  Office
};
