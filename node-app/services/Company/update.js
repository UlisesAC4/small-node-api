const Models = require("../../models");
const Company = Models.company;
const Office = Models.office;

module.exports = async (companyId, officeId) => {
  const company = await Company.findOne({ where: { id: companyId } });
  const office = await Office.findOne({ where: { id: officeId } });

  
  await company.setHeadquarter(office);
  return company;
};
