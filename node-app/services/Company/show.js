const Models = require("../../models");
const Company = Models.company;
const Office = Models.office;
const sequelize = require("sequelize");

module.exports = async companyId => {
  const company = await Company.findOne({
    where: { id: companyId },
    include: [
      {
        model: Office,
        as: "Headquarter",
        attributes: ["street", "postalCode", "city"]
      }
    ]
  });
  const offices = await company.getOffices({
    attributes: [
      [sequelize.fn("SUM", sequelize.col("monthlyRent")), "totalRent"]
    ]
  });

  const result = {};
  (result.name = company.name), (result.headquarter = company.Headquarter);
  result.totalRent = offices[0];

  return result;
};
