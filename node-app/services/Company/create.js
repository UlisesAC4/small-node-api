const Models = require("../../models");
const Company = Models.company;
const Office = Models.office;

module.exports = async companyData => {
  const company = await Company.create({ name: companyData.name });
  const promisedOfficeCreation = companyData.offices.map(office => {
    return Office.create({
      street: office.street,
      postalCode: office.postalCode,
      city: office.city,
      monthlyRent: office.monthlyRent
    });
  });

  const offices = await Promise.all(promisedOfficeCreation);
  await company.setOffices(offices);

  return company;
};
