const create = require("./create");
const show = require("./show");
const update = require("./update");

module.exports = { create, show, update };
