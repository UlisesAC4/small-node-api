const Services = require("../services");

module.exports = {
  create: async (req, res) => {
    const response = await Services.Company.create(req.body);
    res.json(response);
  },
  show: async (req, res) => {
    const response = await Services.Company.show(req.params.companyId);
    res.json(response);
  },
  update: async (req, res) => {
    const response = await Services.Company.update(
      req.params.companyId,
      req.body.officeId
    );
    res.json(response);
  }
};
