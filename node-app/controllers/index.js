const Company = require("./company");
const Office = require("./office");

module.exports = { Company, Office };
