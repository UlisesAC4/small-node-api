const Services = require("../services");

module.exports = {
  showAll: async (req, res) => {
    const response = await Services.Office.showAll(req.params.companyId);
    res.json(response);
  }
};
