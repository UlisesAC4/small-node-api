module.exports = (sequelize, DataTypes) => {
  const Office = sequelize.define(
    "office",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      street: DataTypes.STRING(256),
      postalCode: DataTypes.STRING(32),
      city: DataTypes.STRING(128),
      monthlyRent: DataTypes.DECIMAL(10, 2)
    },
    {}
  );
  Office.associate = models => {
    // associations can be defined here
    models.office.belongsTo(models.company);
  };
  return Office;
};
/*
street = models.CharField('Street', max_length=256, blank=True)
postal_code = models.CharField('Postal Code', max_length=32, blank=True)
city = models.CharField('City', max_length=128, blank=True, null=True)
monthly_rent = models.DecimalField(decimal_places=2, max_digits=10, blank=True, null=True)
*/
