module.exports = (sequelize, DataTypes) => {
  const Company = sequelize.define(
    "company",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: DataTypes.STRING(300)
    },
    {}
  );
  Company.associate = models => {
    // associations can be defined here
    models.company.hasMany(models.office);
    models.company.hasOne(models.office, { as: "Headquarter" });
  };
  return Company;
};
