const Controllers = require("../controllers");
const express = require("express");

const router = express.Router();

router.post("/", Controllers.Company.create);
router.get("/:companyId", Controllers.Company.show);
router.put("/:companyId", Controllers.Company.update);

module.exports = router;
