const Controllers = require("../controllers");
const express = require("express");

const router = express.Router({ mergeParams: true });

router.get("/", Controllers.Office.showAll);

module.exports = router;
