const bodyParser = require("body-parser");
const express = require("express");
const logger = require("morgan");
const cors = require("cors");
const Routes = require("./routes");

const app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use("/companies", Routes.Company);
app.use("/companies/:companyId/offices", Routes.Office);

const Models = require("./models");

(async () => {
  await Models.sequelize.sync({
    force: true
  });
  console.log("Models created correctly");
})();
app.listen(5555, () => console.log("Server is up"));
