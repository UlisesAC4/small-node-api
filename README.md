## Introduction

This is a sample application nodejs api with react frontend application. It is built separating frontend with react + redux and backend with nodejs.

I tried my best to build a rest api in django but I could not with all the tools that already works for you, yet I left the folder if that matters.

## Questions

The following requirements were put in the description, the thing is, how to resolve them and test them?

- # - A Company can have one or more offices. This is done at models definition, one has to ensure that the cardinality is put as **A company has many offices, and an offices belongs to a company**. I have not heard of a testing tool for checking this, my best shot would be write down the associations and check them by hand.

- # - Offices can be an headquarter. This is done at models definition too, I believe that the best approach is pointing an extra association from the company to the offices, only one as the type **A company has one offices as headquarter, one offices belongs to this company as headquarter**. Again I believe that. Only checking it by hand would be able to check this. There are tools that draws the entire database but I have not heard of something unique, I personally use dbeaver just to have a print of the layout of my tables.

- # - A company should at all times have exactly one headquarter. This is answered in the last point.

- # - Write an API endpoint to create the company along with office information. This is the create endpoint, one has to send one object with the name of the company and an array of the number of the offices, the endpoint takes the name, creates the company, then creates the offices and associates them to the company.

- # - Write an simple read-only API endpoint for companies to get the company name + street+postal_code+city from the headquarter office. I put this in show company just to simplify things, in sequelize this is done "Including" a model in the select query, sequelize will put the model if something is associated. I only have to list the attributes needed.

- # - Write an simple read-only API endpoint to get all the offices for a company. This is the endpoint show all offices, it takes the id of the company and sends only the offices of that company, it show all the info of each at the moment.

- # - Write an API endpoint to change the headquarter of the company. This is done in the update company, the only functionality that this endpoint has is to accept the id of the new headquarter and "set it".

- # - Customize the API endpoint to include the sum of rent for all offices of a Company. How would you approach / test this?. I also put this in show company endpoint to save api endpoints. This is a different query, I have to list all the offices and make a sum of each column of the monthly rent and put it in the parent object.

## Backend

The backend is done in nodejs dubnium lts version. It uses sqlite as db so there is no need to install anything else. To launch it just issue `node index.js` in the folder `node-app`.

It has a clear structure, the domain logic is placed in the services module. Then called in the controllers module and assigned in the routes module. 

Each module is exported as one file so one can access each part with dot notation.

## Frontend

Frontend uses redux as specified. It only has one dispatch function, that is to register the info of the created company so the rest of the screens can work.

You have to use the create screen before using update and details. Details will show the name, headquarters and offices.

If no information is found it will display the necessary message. You are redirected to details after creating a company so you won't be able to see the headquarters,in this case just go to update and set it from the offices, of course if you put them in the first place.

This application only saves the current company that you create,  the next time you create another one, the company will be discarded because the store only saves one company.

The front also creates modules to reach deeper modules using dot notation.

To run the front just issue `npm start`

## Closing thoughts

The project ended being a little longer than expected, but it was fun to me. My biggest pain was my low experience on django, it hindered my productivity and could not complete the assignment in the preferred way.

I understand that the inside code is not commented at all, this time as the project is small I preferred to keep it as is, the code is simple enough to be understood with just an overview put in this file. If not raise the critique to me.

Testing would be via jest on nodejs at services and controller level, on react I do not have knowledge on testing it.

Thank your for reading my entry. Have a nice day!