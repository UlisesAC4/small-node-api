from rest_framework import serializers
from offices.models import Office, Company
#from offices.views import OfficeSerializer, CompanySerializer


class OfficeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Office

        exclude = ['company_owner']

    def create(self, validated_data):
        return super().create(validated_data)


class CompanySerializer(serializers.ModelSerializer):
    headquarter = OfficeSerializer(required=False)
    offices = OfficeSerializer(many=True, required=False)

    class Meta:
        model = Company
        fields = '__all__'

    def create(self, validated_data):
        print(Company)
        offices_data = validated_data.pop("offices")
        print('offices ', offices_data, ' normal data ', validated_data)
        company = Company.objects.create(**validated_data)
        for office_data in offices_data:
            Office.objects.create(company_owner=company, **office_data)
        return company
        # return super().create(validated_data)
