from django.shortcuts import render
from offices.models import Office, Company
from offices.serializers import CompanySerializer, OfficeSerializer
from rest_framework import viewsets
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
# Create your views here.


class CompanyListViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    


class OfficeListListViewSet(viewsets.ModelViewSet):
    queryset = Office.objects.all()
    serializer_class = OfficeSerializer
