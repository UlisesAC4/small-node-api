from django.db import models

# Create your models here.


class Office(models.Model):
    street = models.CharField('Street', max_length=256, blank=True)
    postal_code = models.CharField('Postal Code', max_length=32, blank=True)
    city = models.CharField('City', max_length=128, blank=True, null=True)
    monthly_rent = models.DecimalField(
        decimal_places=2, max_digits=10, blank=True, null=True)
    company_owner = models.ForeignKey(
        "Company", on_delete=models.DO_NOTHING, related_name='offices')


class Company(models.Model):
    name = models.CharField('Name', max_length=300)
    headquarter = models.OneToOneField(
        "Office", on_delete=models.DO_NOTHING, blank=True, null=True)
